CREATE USER amazon;
CREATE DATABASE amazon;
ALTER USER amazon WITH PASSWORD 'amazon';
GRANT ALL PRIVILEGES ON DATABASE amazon TO amazon;

CREATE TABLE reviews ( 
    index varchar(100) NOT NULL PRIMARY KEY, 
    data jsonb
);
