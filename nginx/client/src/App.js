import React from 'react';
import { Html5Entities } from 'html-entities';
import axios from 'axios';
import { Form, ListGroup, Card, Badge, Button, Overlay, Tooltip } from 'react-bootstrap';
import StarRatingComponent from 'react-star-rating-component';

const htmlEntities = new Html5Entities();

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            rated: [],
            reviews: [],
            item: [],
            text: '',
            rating: '',
            relevancy: '',
            started: false,
            submitted: false,
            fixEmail: false,
            fixInput: false,
            targetSuccess: null,
            targetNext: null
        };
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.attachRefSuccess = targetSuccess => this.setState({ targetSuccess });
        this.attachRefNext = targetNext => this.setState({ targetNext });

        window.onbeforeunload = function() {
            return "Confirm leaving";
        }
    }

    componentDidMount() {
        console.log("Component mounted");
    }

    
    onStarClick(nextValue, prevValue, name) {
        this.setState({ [name]: nextValue });
    }

    handleEmailChange(event) {
        this.setState({ email: event.target.value });

    }   
    
    handleTextChange(event) {
        this.setState({ text: event.target.value });
    }

    handleBegin() {
        this.setState({ started: true });
        this.interval = setInterval(() => this.setState({
            fixEmail: false,
            fixInput: false
        }), 4000);
        this.handleNewItem();
    }

    handleAgain() {
        this.setState({
            date: '',
            email: '',
            rated: [],
            reviews: [],
            item: [],
            text: '',
            rating: '',
            relevancy: '',
            submitted: false
        });
        this.setState({ started: false });
    }

    handleNext = () => {
        if (this.state.text || this.state.rating || this.state.relevancy) {
            if (this.state.rating && this.state.relevancy) {
                this.setState({ reviews: this.state.reviews.concat([{
                    "asin": this.state.item.asin,
                    "text": this.state.text,
                    "rating": this.state.rating,
                    "relevancy": this.state.relevancy
                }])});
                this.setState({ rated: this.state.rated.concat([
                    this.state.item.asin
                ])});
                this.handleNewItem();
            } else {
                this.setState({fixInput: true});
            }
        } else {
            this.handleNewItem();
        }
    }

    handleNewItem = () => {
        this.setState({ text: '' });
        this.setState({ rating: '' });
        this.setState({ relevancy: '' });
        axios({
            method: 'post',
            url: 'https://mlatlse.kszk.eu/api/v1/item',
            data: this.state.rated,
            headers: {
                'Content-Type': 'application/json',
            }}).then(response => {
                this.setState({ item: response.data });
        });
        window.scrollTo(0, 0);
    }

    checkSubmitResponse(response) {
        return response.data.validated;
    }

    validateEmail() {
        if (/^[a-zA-Z0-9.]+@[a-zA-Z0-9.]+\.[A-Za-z.]+$/.test(this.state.email)) {
            this.setState({fixEmail: false});
            return true
        } else {
            this.setState({fixEmail: true});
            return false
        }
    }

    handleSubmit = () => {
        if (this.validateEmail()) {
            axios({
                method: 'post',
                url: 'https://mlatlse.kszk.eu/api/v1/review',
                data: {
                    "email": this.state.email,
                    "date": Math.floor(Date.now() / 1000),
                    "reviews": this.state.reviews
                },
                headers: {
                    'Content-Type': 'application/json',
                }}).then(response => {
                    if (this.checkSubmitResponse(response)) {
                        window.scrollTo(0, 0);
                        this.setState({ submitted: true });
                    } else {
                        console.log("Rejected");
                    }
                });
        } else {
            this.setState({fixEmail: true});
        }
    }

    render() {
        if (!this.state.started) {
            return (
                <div>
                <br/>
                <Card className="mx-auto" border="secondary" style={{ width: '18rem' }}>
                    <Card.Body>
                    <Card.Title><b>ML@LSE Collection App</b></Card.Title>
                    <br />
                    <h4>What’s this about?</h4>
                    Recommender systems are everywhere and they continuously draw conclusions from interactions we share using the web. But have you ever wondered just how much information is needed to identify your taste in a meaningful way? We will try to answer this question!
                    <br/>
                    <br/>
                    We challenge you to play a little game: we’ll be showing you a stream of books for you to rate. After you submit your reviews, we’ll then try to find other books that you might enjoy and send the results back to you.
                    <br/>
                    <br/>
                    More precisely, we want to explore in a controlled setting the question of how <b>well</b> a machine learning algorithm can predict consumer’s preference and how <b>much</b> information it needs to do so.
                    <br />
                    <br />
                    <h4>How do I play?</h4>
                    You’ll see an image, title, price range and a description of a random book.
                    <ul>
                        <li>Review the current item: give 1-5 rating score and 1-5 score of certainty i.e. how certain you are about this particular rating.</li>
                        <li>Click the <Badge variant="primary">Add</Badge> button to add your review (you will be automatically given a new item to rate). If you click without giving any input, you’ll skip the current item.</li>
                        <li>At any time you can finish and submit you reviews by clicking the <Badge variant="success">Finish</Badge> button.</li>
                    </ul>
                        That’s it! We will get back to you with our recommendations in couple weeks. Stay tuned!
                    <br />
                    <br />
                        For any queries, please write an email to konstanty at kszk dot eu.
                    <br />
                    <br />
                    <center><Button variant="outline-primary" size='lg' onClick={this.handleBegin.bind(this)}>Begin</Button></center>
                    </Card.Body>
                </Card>
                <br/>
                </div>
            )
        } else {
            if (!this.state.submitted) {
                return (
                    <div>
                    <br/>
                    <Card className="mx-auto" border="secondary" style={{ width: '18rem' }}>
                      <Card.Img variant="top" src={"https://images.weserv.nl/?url=" + this.state.item['imurl']} />
                      <Card.Body>
                        <Card.Title>
                            <b>{this.state.item['title']}</b>
                        </Card.Title>
                    <Card.Subtitle>
                        { (0.9673*(parseFloat(this.state.item['price'])/1.33)).toFixed(2) + ' to ' + (1.1157*(parseFloat(this.state.item['price']))).toFixed(2) } USD
                    </Card.Subtitle>
                        <Card.Text>
        {htmlEntities.decode(this.state.item['desc'])}
                        </Card.Text>
                      </Card.Body>
                    <ListGroup variant="flush">
                    <ListGroup.Item>
                        <h4>Rating { this.state.rating }</h4>
                        <div style={{'margin-left': '0px', 
                                    'margin-bottom':'-20px', 
                                    'font-size': '48px'}}>
                        <StarRatingComponent
                            name="rating"
                            starCount={ 5 }
                            value={ parseInt(this.state.rating) }
                            onStarClick={this.onStarClick.bind(this)}/>
                        </div>
                        </ListGroup.Item>
                    <ListGroup.Item>
                        <h4>Certainty { this.state.relevancy }</h4>
                        <div style={{'margin-left': '0px', 
                                    'margin-bottom':'-20px', 
                                    'font-size': '48px'}}>
                        <StarRatingComponent
                            name="relevancy"
                            starCount={ 5 }
                            value={ parseInt(this.state.relevancy) }
                            onStarClick={this.onStarClick.bind(this)}/>
                        </div>
                    </ListGroup.Item>
                    <ListGroup.Item>
                    <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control value={this.state.email} type="email" onChange={this.handleEmailChange} placeholder="name@example.com" />
                    </Form.Group>
                    </ListGroup.Item>
                    <ListGroup.Item>
                    <Button onClick={this.handleNext} ref={this.attachRefNext}
                    variant="primary">Add
                    </Button>
                    <Overlay target={this.state.targetNext}
                    show={this.state.fixInput} placement="down">
                        {props => (
                            <Tooltip id="overlay-example" {...props}>
                                Please fill both rating and certainty!
                            </Tooltip>
                        )}
                    </Overlay>
                    &nbsp; 
                    <Button onClick={this.handleSubmit} ref={this.attachRefSuccess}
                    variant="success">
                    Finish with <Badge variant="light">{this.state.rated.length}</Badge> items</Button>
                    <Overlay target={this.state.targetSuccess} 
                    show={this.state.fixEmail} placement="down">
                        {props => (
                            <Tooltip id="overlay-example" {...props}>
                                Please provide a proper email address!
                            </Tooltip>
                        )}
                    </Overlay>
                    </ListGroup.Item>
                    </ListGroup>
                    </Card>
                    <br/>
                    </div>
                    );
                } else {
                    return (
                        <div>
                        <br/>
                        <Card className="mx-auto" border="secondary" style={{ width: '18rem' }}>
                            <Card.Body>
                            <Card.Title>Thanks a lot {this.state.email}!</Card.Title>
                            <Card.Subtitle>You submitted <Badge variant="success">{this.state.rated.length}</Badge> items.</Card.Subtitle>
                            <Card.Text>We will get back to you soon.</Card.Text>
                            <Button onClick={this.handleAgain.bind(this)}>Start over</Button>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">
                            We care about your privacy: we only store your email address to have a way to contact 
                            you about the recommendations. Otherwise we will use an anonymous identifier.
                            </small>
                            </Card.Footer>
                        </Card>
                        <br/>
                        </div>
                    );
                }
        }
    }
}
