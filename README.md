# Instructions

To build React app navigate to `nginx/source` and run `yarn build`.

Assuming you obtained correct certificates and have `.env` populated with relevant variables, execute `run.sh` to deploy.

The PostgreSQL schema sets up only the review receiving end. In order to have a fully working environment, construct `items` table as described in the `api/app.py` ItemModel class and manually populate it with items data. 
