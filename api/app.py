import os
import time
import hashlib
from random import randint
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api
from flask_restful.utils import cors
from sqlalchemy.dialects.postgresql.json import JSONB

DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(
        user=os.environ['POSTGRES_USER'],
        pw=os.environ['POSTGRES_PASSWORD'],
        url=os.environ['POSTGRES_URL'],
        db=os.environ['POSTGRES_DB'])

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
api = Api(app, prefix='/api/v1')

class ItemModel(db.Model):
    __tablename__ = 'items'
    index = db.Column(db.Integer, primary_key=True)
    asin = db.Column(db.String(20), nullable=False, unique=True)
    title = db.Column(db.String(100), nullable=True, unique=False)
    desc = db.Column(db.String(500), nullable=True, unique=False)
    price = db.Column(db.Float, nullable=True, unique=False)
    imurl = db.Column(db.String(100), nullable=True, unique=False)

class ReviewModel(db.Model):
    __tablename__ = 'reviews'
    index = db.Column(db.String(100), primary_key=True)
    data = db.Column(JSONB)

class ItemEndpoint(Resource):
    @cors.crossdomain(origin='*')
    def post(self):
        rated = request.get_json()
        item = None
        while item is None:
            item = db.session.query(ItemModel).filter(
                    ItemModel.index == randint(1, 24343)).first()
            if item.asin in rated:
                item = None
        return jsonify({
                'asin': item.asin,
                'title': item.title,
                'desc': item.desc,
                'price': item.price,
                'imurl': item.imurl
                })

def append(reviews):
    id_ = str(reviews['email']) + str(reviews['date'])
    m = hashlib.sha1()
    m.update(id_.encode('utf-8'))
    id_hashed = m.hexdigest()

    data = {
            'email': reviews['email'],
            'date': reviews['date'],
            'reviews': reviews['reviews']
            }

    db.session.add(ReviewModel(index=id_hashed, data=data))
    try:
        db.session.commit()
    except Exception as err:
        return False, err
    return True, data

class ReviewEndpoint(Resource):
    @cors.crossdomain(origin='*')
    def post(self):
        reviews = request.get_json()
        response, content = append(reviews)
        return jsonify({'validated': response, 'content': content})

api.add_resource(ItemEndpoint, '/item')
api.add_resource(ReviewEndpoint, '/review')

if __name__ == '__main__':
    app.run(port=8000, host='0.0.0.0')
